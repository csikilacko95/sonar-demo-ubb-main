let a = 0;
let b = 0;
let results = 0;

const aElement = document.getElementById('a-number');
const bElement = document.getElementById('b-number');
const resultsElement = document.getElementById('results');

const usernameElement = document.getElementById('username');
const passwordElement = document.getElementById('password');


void function init() {
  const password = 'test';
  console.log(password);
  alert(password);
  if (a == 0) {
    console.log(a);
  }
  duplicatedCalculate();
  btnAlertFunction();
  NaNTest();
  operatorTest();
  ifTest();
  deleteTest();
  webSQL();
}

function btnAlertFunction() {
  alert('my dummy alert!');
}

function calculate() {
  a = aElement.value;
  b = bElement.value;
  console.log('a length:', a.length);
  console.log(a + b);
  if (a && b) {
    results = a + b;
  }
  resultsElement.innerHTML = results;
}

function duplicatedCalculate() {
  a = aElement.value;
  b = bElement.value;
  console.log('a length:', a.length);
  console.log(a + b);
  if (a && b) {
    results = a + b;
  }
  resultsElement.innerHTML = results;
}

function NaNTest() {
  let c = NaN;

  if (c === NaN) {  // Noncompliant; always false
    console.log('c is not a number');  // this is dead code
  }
  if (c !== NaN) { // Noncompliant; always true
    console.log('c is not NaN'); // this statement is not necessarily true
  }
}

function operatorTest() {
  let a =-5;
  let b = 3;

  a =- a;  // Noncompliant; target = -3. Is that really what's meant?
  b =+ b; // Noncompliant; target = 3
}


function ifTest() {
  if (a === 1)
    helperOne();
  else if (a === 2)
    helperTwo();
  else if (a === 1)  // Noncompliant
    helperThree();
}

function helperOne() {
  console.log('helper one');
}
function helperTwo() {
  console.log('helper two');
}
function helperThree() {
  console.log('helper three');
}


function deleteTest() {
  var myArray = ['a', 'b', 'c', 'd'];

  delete myArray[2];  // Noncompliant. myArray => ['a', 'b', undefined, 'd']
  console.log(myArray[2]); // expected value was 'd' but output is undefined

  //Complient version
  // removes 1 element from index 2
  //myArray.splice(2, 1);  // myArray => ['a', 'b', 'd']
  //console.log(myArray[2]); // outputs 'd'
}


function login() {
  localStorage.setItem("username", usernameElement.value); // Noncompliant
  localStorage.setItem("password", passwordElement.value); // Noncompliant
  sessionStorage.setItem("sessionId", 1); // Noncompliant
}

function webSQL(){
  const db = window.openDatabase("myDb", "1.0", "Personal secrets stored here", 2*1024*1024);  // Noncompliant
  db.changeVersion("1.0","2.0");
}
